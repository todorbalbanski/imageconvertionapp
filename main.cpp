#include "startup.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    StartUp startUp;
    startUp.show();

    return a.exec();
}
