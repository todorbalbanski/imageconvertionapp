#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ImageControllers/imagecontroller.h>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QFile>
#include <QLabel>
#include <QActionGroup>
#include <QMenu>
#include <QMenuBar>
#include <QListWidget>
#include <QStringList>
#include <QStringListModel>
#include <QPushButton>
#include <QProgressBar>
#include <QComboBox>


#include "loadimageswindow.h"
#include <map>

using namespace std;

enum ImageEffects { grayscale = 0 , blurred = 1};
extern map<QString, ImageEffects> mapImageEffects;


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Q_ENUMS(ImageEffects)

public:
    explicit MainWindow(QWidget *parent);
    ~MainWindow();
    //ProgressBar
    QProgressBar *progressBar;

private:
    Ui::MainWindow *ui;
    QFile* m_file;
    QLabel* m_chooseOption;
    QWidget* w;
    QListView * m_listView;
    QList<QString> m_allImages;

    //menu
    QMenu *m_fileMenu;
    QActionGroup* m_alignmentGroup;
    QAction* m_LoadImages;
    QAction* m_ExitAct;
    QStringListModel* m_model;

    //Images
    QLabel* m_imageLabel;
    QLabel* m_imagePreviewLabel;
    QLabel* m_imageText;
    QLabel* m_imagePreviewText;

    //ComboBox
    QComboBox* m_chooseEffect;

    QHBoxLayout* m_hLayout;
    QHBoxLayout* m_vLayout;
    QVBoxLayout* m_mainLayout;

    QPushButton *m_buttonBrowse;
    QPushButton *m_buttonApply;


private:
    void registerFilePaths();
    void createMenus();
    void createActions();
    void setlayouts();
    void exit();

public slots:
    void fillListWidget(QStringList aListString);
    void loadImages();
    void loadImageToScreen(const QModelIndex & current);
    void setImageToPreviewScreen(QImage aImage);
    void applyEffectToAllImages();
    void setSlots();

signals:
    void NotifyImagesAreLoaded(QStringList aListString);
    void NotifyImageIsReadyToPreview(QImage aImage);
    void NotifyTotalImages(int);
    void ConvertAllImagesAndSave(QList<QString> aImages, int effekt);
    QImage ApplyGrayScaleEffectToImage(QImage aImage);
    QImage ApplyBlurredEffectToImage(QImage aImage);

};

#endif // MAINWINDOW_H
