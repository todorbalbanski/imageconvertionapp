#include "mainwindow.h"
#include <QFileInfo>
#include <QFileDialog>
#include <QList>
#include <QString>
#include <QImage>
#include <QDebug>

map<QString, ImageEffects> mapImageEffects;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    m_model = new QStringListModel(this);
    m_listView = new QListView(this);

    registerFilePaths();

    m_buttonBrowse = new QPushButton(nullptr);
    m_buttonBrowse->setText("Browse");
    connect(m_buttonBrowse, SIGNAL(clicked()), this, SLOT(loadImages()));

    m_buttonApply = new QPushButton(nullptr);
    m_buttonApply->setText("ApplyToAll");

    m_chooseEffect = new QComboBox(this);
    m_chooseEffect->setSizeAdjustPolicy(QComboBox::AdjustToContentsOnFirstShow);
    m_chooseEffect->addItem("grayscale", grayscale);
    m_chooseEffect->addItem("blurred", blurred);

    //Image Text
    m_imageText = new QLabel;
    m_imageText->setText("Actual Picture");
    m_imagePreviewText = new QLabel;
    m_imagePreviewText ->setText("Preview");
    m_imageLabel = new QLabel;
    m_imagePreviewLabel = new QLabel;

    progressBar = new QProgressBar(this);

    createActions();
    createMenus();
    setlayouts();

    w = new QWidget();
    w->setLayout(m_mainLayout);
    setCentralWidget(w);

    setSlots();
}

MainWindow::~MainWindow()
{

}


void MainWindow::createMenus()
{
    m_fileMenu = menuBar()->addMenu(tr("&File"));
    m_fileMenu->addAction(m_LoadImages);
    m_fileMenu->addSeparator();
    m_fileMenu->addAction(m_ExitAct);
}

void MainWindow::createActions()
{
    m_LoadImages = new QAction(tr("&Load..."), this);
    m_LoadImages->setShortcuts(QKeySequence::New);
    m_LoadImages->setStatusTip(tr("LoadFromXml"));
    connect(m_LoadImages, &QAction::triggered, this, &MainWindow::loadImages);

    m_ExitAct = new QAction(tr("&Exit"), this);
    m_ExitAct->setStatusTip(tr("Exit"));
    connect(m_ExitAct, &QAction::triggered, this, &MainWindow::exit);
}

void MainWindow::setlayouts()
{
    m_hLayout = new QHBoxLayout;
    m_vLayout = new QHBoxLayout;

    m_hLayout->addWidget(m_buttonBrowse);
    m_hLayout->addWidget(m_chooseEffect);
    m_hLayout->addWidget(m_buttonApply);
    m_hLayout->addStretch(1);
    m_hLayout->addWidget(m_imageText);
    m_hLayout->addStretch(1);
    m_hLayout->addWidget(m_imagePreviewText);
    m_hLayout->addStretch(1);

    m_vLayout->addWidget(m_listView);

    m_vLayout->addWidget(m_imageLabel);
    m_vLayout->addWidget(m_imagePreviewLabel);

    m_mainLayout = new QVBoxLayout;
    m_mainLayout->addLayout(m_hLayout);
    m_mainLayout->addLayout(m_vLayout);
    m_mainLayout->addWidget(progressBar);
}

void MainWindow::loadImages()
{
    m_allImages = QList<QString>();
    const QStringList fileNames = QFileDialog::getOpenFileNames(this,tr("Image Files (*.png *.jpg *.bmp)"),"D:/Projects/Demo-Qt-Apps/ImageConvertionApp/bmpImages",
                                                                tr("Image Files (*.png *.jpg *.bmp)") );
    if( !fileNames.isEmpty() )
    {
        for (int i =0;i<fileNames.count();i++)
            //ui->lstFiles->addItem(filenames.at(i));
            m_allImages.append(fileNames.at(i));
    }

    connect(this, SIGNAL(NotifyTotalImages(int)), progressBar, SLOT(setMaximum(int)));
    emit NotifyTotalImages(m_allImages.count());
    emit NotifyImagesAreLoaded(fileNames);
}

void MainWindow::loadImageToScreen(const QModelIndex & current)
{
      int selectedImage = current.row();
      QImage image(m_allImages.at(selectedImage));
      image.scaled(500,500,Qt::IgnoreAspectRatio);
      m_imageLabel->setPixmap(QPixmap::fromImage(image));

      emit NotifyImageIsReadyToPreview(image);
}

void MainWindow::setImageToPreviewScreen(QImage aImage)
{
    QImage image;
    int selectedEffect = m_chooseEffect->itemData(m_chooseEffect->currentIndex()).toInt();
    switch (selectedEffect) {
        case grayscale:
        {
            image = ApplyGrayScaleEffectToImage(aImage);
            break;
        }
        case blurred:
        {
            image = ApplyBlurredEffectToImage(aImage);
            break;
        }
    }

    m_imagePreviewLabel->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::applyEffectToAllImages()
{
    if(m_allImages.isEmpty())
    {
        return;
    }

    int effekt = m_chooseEffect->itemData(m_chooseEffect->currentIndex()).toInt();
    emit ConvertAllImagesAndSave(m_allImages, effekt);
}

void MainWindow::setSlots()
{
    connect(this, SIGNAL(NotifyImagesAreLoaded(QStringList)), this, SLOT(fillListWidget(QStringList)));
    connect(m_listView,SIGNAL(doubleClicked(QModelIndex)),
            this, SLOT(loadImageToScreen(QModelIndex)));

    connect(this, SIGNAL(NotifyImageIsReadyToPreview(QImage)), this, SLOT(setImageToPreviewScreen(QImage)));
    connect(m_buttonApply, SIGNAL(clicked(bool)), this, SLOT(applyEffectToAllImages()));
}


void MainWindow::exit()
{
    this->close();
}

void MainWindow::registerFilePaths(){
    mapImageEffects["grayscale"] = grayscale;
    mapImageEffects["blurred"] = blurred;
}

void MainWindow::fillListWidget(QStringList aListString)
{
    m_model->setStringList(aListString);
    m_listView->setModel(m_model);
}
