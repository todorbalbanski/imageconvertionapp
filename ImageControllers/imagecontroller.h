#ifndef IMAGECONTROLLER_H
#define IMAGECONTROLLER_H
#include <QObject>
#include <QImage>
#include <QPixmap>
#include <QLabel>


class ImageController : public QObject
{
    Q_OBJECT
public:
    explicit ImageController(QObject *parent = 0);

private:
    void saveImage(QString aUrlPath, int aEffect, int& i);

signals:
    void progress(int);

public slots:
    QImage applyGrayScaleEffect( QImage aImageToGrayScale);
    QImage applyBlurEffect(QImage aSrc, int extent = 0);
    void convertAllImagesAndSave(const QList<QString> aImages, int aEffect);

private:
    QImage applyEffectToImage(QImage aSrc, QGraphicsEffect *effect, int extent = 0);

};

#endif // IMAGECONTROLLER_H
