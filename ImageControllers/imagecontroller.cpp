#include "imagecontroller.h"
#include <QPainter>
#include <QGraphicsBlurEffect>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QFile>
#include <QFileInfo>
#include <QByteArray>
#include <QBuffer>
#include <QDate>
#include <QDebug>
#include <QFuture>
#include <QThread>
#include <QtConcurrent>
#include <qtconcurrentrun.h>

ImageController::ImageController(QObject *parent) : QObject(parent)
{
}

void ImageController::saveImage(QString aUrlPath, int aEffect, int &i)
{
    qDebug()<<"Concurrent:" <<QThread::currentThread();
    QImage image(aUrlPath);

    QFile file(aUrlPath);
    QFileInfo* imageFilePath = new QFileInfo(file);
    QString extension = imageFilePath->suffix();

    QDateTime date;
    QString dates = date.currentDateTime().toLocalTime().toString("yyyy-MM-dd_hh-mm");
    QString imagePathToSave;
    switch (aEffect) {
        case 0:
        {
            image = applyGrayScaleEffect(image);
            QString relDir = "D:/Projects/Demo-Qt-Apps/ImageConvertionApp/EdittedImages/grayscaled/";
            imagePathToSave = relDir + imageFilePath->baseName() + "_" + dates + "." + imageFilePath->suffix();
            break;
        }
        case 1:
        {
            image = applyBlurEffect(image);
            QString relDir = "D:/Projects/Demo-Qt-Apps/ImageConvertionApp/EdittedImages/blurred/";
            imagePathToSave = relDir + imageFilePath->baseName() + "_" + dates + "." + imageFilePath->suffix();
            break;
        }
    }
    image.save(imagePathToSave);
}

QImage ImageController::applyGrayScaleEffect(QImage aImageToGrayScale)
{
    QImage image = aImageToGrayScale;

            for (int ii = 0; ii < image.width(); ii++) {
                for (int jj = 0; jj < image.height(); jj++) {
                    int gray = qGray(image.pixel(ii, jj));
                    image.setPixel(ii, jj, QColor(gray, gray, gray).rgb());
                }
            }

            return image;
}


QImage ImageController::applyEffectToImage(QImage aSrc, QGraphicsEffect *effect, int extent)
{
    if(aSrc.isNull()) return QImage();   //No need to do anything else!
    if(!effect) return aSrc;             //No need to do anything else!
    QGraphicsScene scene;
    QGraphicsPixmapItem item;
    item.setPixmap(QPixmap::fromImage(aSrc));
    item.setGraphicsEffect(effect);
    scene.addItem(&item);
    QImage res(aSrc.size()+QSize(extent*2, extent*2), QImage::Format_ARGB32);
    res.fill(Qt::transparent);
    QPainter ptr(&res);
    scene.render(&ptr, QRectF(), QRectF( -extent, -extent, aSrc.width()+extent*2, aSrc.height()+extent*2 ) );
    return res;
}


QImage ImageController::applyBlurEffect(QImage aSrc, int extent)
{
    QGraphicsBlurEffect *blur = new QGraphicsBlurEffect;
    blur->setBlurRadius(8);
    QImage res = applyEffectToImage(aSrc, blur, 40);
    return res;
}


void ImageController::convertAllImagesAndSave( const QList<QString> aImages, int aEffect)
{
    QString urlPath;
    int i = 1;

    foreach (urlPath, aImages) {
            qDebug()<< "current:" << QThread::currentThread();
           QFuture<void> execute = QtConcurrent::run(this, &ImageController::saveImage, urlPath, aEffect, i);
           emit progress(i);

           execute.waitForFinished();

        i++;
    }
}
