#include "startup.h"

StartUp::StartUp(QObject *parent) : QObject(parent), m_mainwindow(Q_NULLPTR)
{
    setupEvents();
}

void StartUp::setupEvents()
{
    connect(&m_imageController, SIGNAL(progress(int)), m_mainwindow.progressBar, SLOT(setValue(int)));
    connect(&m_mainwindow, SIGNAL(ApplyGrayScaleEffectToImage(QImage)), &m_imageController, SLOT(applyGrayScaleEffect(QImage)));
    connect(&m_mainwindow, SIGNAL(ConvertAllImagesAndSave(QList<QString>,int)), &m_imageController, SLOT(convertAllImagesAndSave(QList<QString>,int)));
}

void StartUp::show()
{
    m_mainwindow.show();
}
