#ifndef STARTUP_H
#define STARTUP_H

#include "mainwindow.h"
#include "ImageControllers/imagecontroller.h"

#include <QObject>

class StartUp : public QObject
{
    Q_OBJECT
public:
    explicit StartUp(QObject *parent = 0);
    void show();

private:
    void setupEvents();

private:
    MainWindow m_mainwindow;
    ImageController m_imageController;

signals:

public slots:
};

#endif // STARTUP_H
